import java.util.Scanner;

class Solution {
    public int numPairsDivisibleBy60(int[] time) {
        int[] remainderCount = new int[60];
        int count = 0;

        for (int t : time) {
            int remainder = t % 60;
            int complement = (60 - remainder) % 60;
            count += remainderCount[complement];
            remainderCount[remainder]++;
        }

        return count;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter the number of songs: ");
        int n = scanner.nextInt();
        int[] time = new int[n];

        System.out.println("Enter the durations of songs:");
        for (int i = 0; i < n; i++) {
            time[i] = scanner.nextInt();
        }

        Solution solution = new Solution();
        int result = solution.numPairsDivisibleBy60(time);

        System.out.println("Number of pairs with total duration divisible by 60: " + result);

        // Close the scanner
        scanner.close();
    }
}
