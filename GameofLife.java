class Solution {
    public void gameOfLife(int[][] board) {
        int m = board.length;
        int n = board[0].length;
        
        int[][] newBoard = new int[m][n];
        
        int[] dx = {-1, -1, -1, 0, 0, 1, 1, 1};
        int[] dy = {-1, 0, 1, -1, 1, -1, 0, 1};
        
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                int liveNeighbors = 0;
                
                // Count live neighbors
                for (int k = 0; k < 8; k++) {
                    int x = i + dx[k];
                    int y = j + dy[k];
                    if (x >= 0 && x < m && y >= 0 && y < n && board[x][y] == 1) {
                        liveNeighbors++;
                    }
                }
                
                // Apply rules
                if (board[i][j] == 1) {
                    if (liveNeighbors < 2 || liveNeighbors > 3) {
                        newBoard[i][j] = 0; // Cell dies
                    } else {
                        newBoard[i][j] = 1; // Cell lives
                    }
                } else {
                    if (liveNeighbors == 3) {
                        newBoard[i][j] = 1; // Cell becomes alive
                    } else {
                        newBoard[i][j] = 0; // Cell remains dead
                    }
                }
            }
        }
        
        // Update original board with new state
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                board[i][j] = newBoard[i][j];
            }
        }
    }
}
